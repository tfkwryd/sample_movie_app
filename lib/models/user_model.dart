class UserModel {
  int? id;
  String? email;
  String? userName;
  String? password;

  UserModel({this.id, this.email, this.userName, this.password});

  UserModel copy({
    int? id,
    String? email,
    String? userName,
    String? password,
  }) =>
      UserModel(
        id: id ?? this.id,
        email: email ?? this.email,
        userName: userName ?? this.userName,
        password: password ?? this.password,
      );

  UserModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        email = json['email'],
        password = json['password'],
        userName = json['username'];

  Map<String, dynamic> toJson() =>
      {'id': id, 'email': email, 'password': password, 'username': userName};
}

const String tableUser = 'user';

class UserFields {
  static List<String> values = [
    id,
    username,
    email,
    password,
  ];
  static String username = 'username';
  static String email = 'email';
  static String password = 'password';
  static String id = 'id';
}
