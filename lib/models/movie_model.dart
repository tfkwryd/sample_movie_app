class MovieModel {
  final String title;
  final String poster;

  MovieModel({required this.title, required this.poster});

  MovieModel.fromJson(Map<String, dynamic> json)
      : title = json['Title'],
        poster = json['Poster'];

  Map<String, dynamic> toJson() => {'Title': title, 'Poster': poster};
}
