// To parse this JSON data, do
//
//     final movieResponseModel = movieResponseModelFromJson(jsonString);

import 'dart:convert';

MovieResponseModel movieResponseModelFromJson(String str) =>
    MovieResponseModel.fromJson(json.decode(str));

String movieResponseModelToJson(MovieResponseModel data) =>
    json.encode(data.toJson());

class MovieResponseModel {
  MovieResponseModel({
    required this.data,
    required this.q,
    required this.v,
  });

  List<Data> data;
  String q;
  int v;

  factory MovieResponseModel.fromJson(Map<String, dynamic> json) =>
      MovieResponseModel(
        data: List<Data>.from(json["d"].map((x) => Data.fromJson(x))),
        q: json["q"],
        v: json["v"],
      );

  Map<String, dynamic> toJson() => {
        "d": List<dynamic>.from(data.map((x) => x.toJson())),
        "q": q,
        "v": v,
      };
}

class Data {
  Data({
    this.i,
    required this.id,
    required this.l,
    this.q,
    this.rank,
    required this.s,
    this.v,
    this.vt,
    this.y,
    this.yr,
  });

  I? i;
  String id;
  String? l;
  String? q;
  int? rank;
  String? s;
  List<V>? v;
  int? vt;
  int? y;
  String? yr;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        i: json["i"] == null ? null : I.fromJson(json["i"]),
        id: json["id"],
        l: json["l"] ?? null,
        q: json["q"] ?? null,
        rank: json["rank"] ?? null,
        s: json["s"] ?? null,
        v: json["v"] == null
            ? null
            : List<V>.from(json["v"].map((x) => V.fromJson(x))),
        vt: json["vt"] ?? null,
        y: json["y"] ?? null,
        yr: json["yr"] ?? null,
      );

  Map<String, dynamic> toJson() => {
        "i": i == null ? null : i!.toJson(),
        "id": id,
        "l": l ?? null,
        "q": q ?? null,
        "rank": rank ?? null,
        "s": s ?? null,
        "v": v == null ? null : List<dynamic>.from(v!.map((x) => x.toJson())),
        "vt": vt ?? null,
        "y": y ?? null,
        "yr": yr ?? null,
      };
}

class I {
  I({
    this.height,
    required this.imageUrl,
    this.width,
  });

  int? height;
  String imageUrl;
  int? width;

  factory I.fromJson(Map<String, dynamic> json) => I(
        height: json["height"] ?? null,
        imageUrl: json["imageUrl"],
        width: json["width"] ?? null,
      );

  Map<String, dynamic> toJson() => {
        "height": height,
        "imageUrl": imageUrl,
        "width": width,
      };
}

class V {
  V({
    required this.i,
    required this.id,
    required this.l,
    required this.s,
  });

  I i;
  String id;
  String l;
  String s;

  factory V.fromJson(Map<String, dynamic> json) => V(
        i: I.fromJson(json["i"]),
        id: json["id"],
        l: json["l"],
        s: json["s"],
      );

  Map<String, dynamic> toJson() => {
        "i": i.toJson(),
        "id": id,
        "l": l,
        "s": s,
      };
}
