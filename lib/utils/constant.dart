class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class DefaultSetting {
  static const keyword = 'Hulk';
}

class Font {}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}

class AssetPath {
  static const noImageAvailable = 'assets/images/image-no-available.png';
}
