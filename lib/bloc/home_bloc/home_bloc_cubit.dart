import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_response_model.dart';
import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/utils/connectivity.dart';
import 'package:majootestcase/utils/constant.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());
  String _keyword = DefaultSetting.keyword;
  set keyword(String value) {
    this._keyword = value;
  }

  String get keyword => _keyword;

  void fetchingData() async {
    emit(HomeBlocInitialState());
    emit(HomeBlocLoadingState());
    if (await ConnectivityUtils.checkNetwork()) {
      ApiServices apiServices = ApiServices();
      if (_keyword != '') {
        try {
          MovieResponseModel? movieResponse =
              await apiServices.getMovieList(_keyword);
          if (movieResponse == null) {
            emit(HomeBlocErrorState("Error Unknown"));
          } else {
            emit(HomeBlocLoadedState(movieResponse.data));
          }
        } catch (e) {
          emit(HomeBlocErrorState(e.toString()));
        }
      } else {
        emit(HomeBlocKeywordNotSetState());
      }
    } else {
      emit(HomeBlocConnectionErrorState("Koneksi internet error"));
    }
  }
}
