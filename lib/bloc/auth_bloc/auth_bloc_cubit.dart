import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user_model.dart';
import 'package:majootestcase/services/db/user_sql.dart';
import 'package:shared_preferences/shared_preferences.dart';
part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void logoutUser() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    await sharedPreferences.setBool("is_logged_in", false);
    await sharedPreferences.setString("user_value", '');
    emit(AuthBlocLoginState());
  }

  void loginUser(UserModel user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    try {
      if (await UserSql.checkUserByEmailPassword(
        user.email.toString(),
        user.password.toString(),
      )) {
        await sharedPreferences.setBool("is_logged_in", true);
        String data = user.toJson().toString();
        await sharedPreferences.setString("user_value", data);
        emit(AuthBlocSuccesState());
        await Future.delayed(const Duration(seconds: 1));
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocFailedState());
      }
    } catch (e) {
      emit(AuthBlocErrorState(e.toString()));
    }
  }

  void registrasiUser(UserModel user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    try {
      await UserSql.create(user).then((value) async {
        await sharedPreferences.setBool("is_logged_in", true);
        String data = value.toJson().toString();
        await sharedPreferences.setString("user_value", data);
        emit(AuthBlocSuccesState());
        await Future.delayed(const Duration(seconds: 1));
        emit(AuthBlocLoggedInState());
      });
    } catch (e) {
      emit(AuthBlocErrorState(e.toString()));
    }
  }
}
