import 'dart:async';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

late Dio dioInstance;

createInstance() async {
  var options = BaseOptions(
      baseUrl: "https://imdb8.p.rapidapi.com/auto-complete?q=game%20of%20thr",
      connectTimeout: 12000,
      receiveTimeout: 12000,
      sendTimeout: 12000,
      headers: {
        "x-rapidapi-key": "5b34e0fc96msh42aee2ae0e52396p1bece6jsn507bcce233c6",
        "x-rapidapi-host": "imdb8.p.rapidapi.com"
      });
  dioInstance = new Dio(options);
  dioInstance.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));
}

Future<Dio> getMovieByKeyword(String keyword) async {
  await createInstance();
  dioInstance.options.baseUrl =
      Uri.parse("https://imdb8.p.rapidapi.com/auto-complete?q=$keyword")
          .toString();
  return dioInstance;
}
