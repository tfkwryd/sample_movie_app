import 'package:majootestcase/models/user_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
// import 'package:alarm/models/alarm_model.dart';
// import 'package:alarm/models/clock_model.dart';

class MovieAppDB {
  static final MovieAppDB instance = MovieAppDB._init();
  static Database? _database;

  MovieAppDB._init();

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDB('movie_app.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);
    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    const idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    const textType = 'TEXT NOT NULL';
    // const blopType = 'BLOB NOT NULL';
    // const integerType = 'INTEGER NOT NULL';
    await db.execute('''
    CREATE TABLE $tableUser (
      ${UserFields.id} $idType,
      ${UserFields.username} $textType,
      ${UserFields.email} $textType,
      ${UserFields.password} $textType
    )
    ''');
  }

  Future close() async {
    final db = await MovieAppDB.instance.database;
    db.close();
  }
}
