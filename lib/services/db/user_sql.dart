import 'package:majootestcase/models/user_model.dart';
import 'movie_app_db.dart';

class UserSql {
  static Future<UserModel> create(UserModel user) async {
    final db = await MovieAppDB.instance.database;
    final id = await db.insert(tableUser, user.toJson());
    return user.copy(id: id);
  }

  static Future<UserModel> read(int id) async {
    final db = await MovieAppDB.instance.database;
    final maps = await db.query(
      tableUser,
      columns: UserFields.values,
      where: '${UserFields.id} = ?',
      whereArgs: [id],
    );
    if (maps.isNotEmpty) {
      return UserModel.fromJson(maps.first);
    } else {
      throw Exception('ID $id not found');
    }
  }

  static Future<bool> checkUserByEmailPassword(
      String email, String password) async {
    final db = await MovieAppDB.instance.database;
    final maps = await db.query(
      tableUser,
      columns: UserFields.values,
      where:
          '${UserFields.email} = "$email" and ${UserFields.password} = "$password"',
    );
    if (maps.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  static Future<List<UserModel>> readAll() async {
    final db = await MovieAppDB.instance.database;
    final orderBy = '${UserFields.id} DESC';
    final result = await db.query(tableUser, orderBy: orderBy);
    return result.map((json) => UserModel.fromJson(json)).toList();
  }

  static Future<int> update(UserModel user) async {
    final db = await MovieAppDB.instance.database;
    return db.update(
      tableUser,
      user.toJson(),
      where: '${UserFields.id} = ?',
      whereArgs: [user.id],
    );
  }

  static Future<int> delete(int id) async {
    final db = await MovieAppDB.instance.database;
    return await db.delete(
      tableUser,
      where: '${UserFields.id} = ?',
      whereArgs: [id],
    );
  }

  Future close() async {
    final db = await MovieAppDB.instance.database;
    db.close();
  }
}
