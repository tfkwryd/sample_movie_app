import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_response_model.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<MovieResponseModel?> getMovieList(String keyword) async {
    try {
      var dio = await dioConfig.getMovieByKeyword(keyword);
      Response<String> response = await dio.get("");
      MovieResponseModel? movieResponse = response.data != null
          ? MovieResponseModel.fromJson(jsonDecode(response.data!))
          : null;
      return movieResponse;
    } on DioError catch (e) {
      throw (e.error);
      // return null;
    }
  }
}
