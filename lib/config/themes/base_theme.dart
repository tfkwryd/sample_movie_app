import 'package:flutter/material.dart';

const Color dangerColor = Color(0xFFC11044);
const Color successColor = Color(0xFF1FC834);
const Color warningColor = Color(0xFFF3A430);

const Color _primaryColor = Color(0xFFFE4B1F);
const Color _primaryVariantColor = Color(0xFFF9A895);
const Color _secondaryColor = Color(0xFF1E1E1E);
const Color _secondaryVariantColor = Color(0xFF181818);
const Color _backgroundColor = Color(0xFF101010);
const Color _surfaceColor = Color(0xFFFEFEFE);
const Color _defaultTextColor = _surfaceColor;

ColorScheme _colorScheme() {
  return const ColorScheme(
    primary: _primaryColor,
    primaryVariant: _primaryVariantColor,
    secondary: _secondaryColor,
    secondaryVariant: _secondaryVariantColor,
    surface: _surfaceColor,
    background: _backgroundColor,
    error: dangerColor,
    onPrimary: _primaryColor,
    onSecondary: _secondaryColor,
    onSurface: _surfaceColor,
    onBackground: _backgroundColor,
    onError: dangerColor,
    brightness: Brightness.light,
  );
}

TextTheme _textTheme() {
  return const TextTheme(
    headline1: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 24,
      fontWeight: FontWeight.w500,
      color: _defaultTextColor,
    ),
    headline2: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 20,
      fontWeight: FontWeight.w500,
      color: _defaultTextColor,
    ),
    headline3: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 18,
      fontWeight: FontWeight.w500,
      color: _defaultTextColor,
    ),
    headline4: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 16,
      fontWeight: FontWeight.w500,
      color: _defaultTextColor,
    ),
    headline5: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 14,
      fontWeight: FontWeight.w500,
      color: _defaultTextColor,
    ),
    headline6: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 12,
      fontWeight: FontWeight.w500,
      color: _defaultTextColor,
    ),
    bodyText1: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 16,
      fontWeight: FontWeight.w400,
      color: _defaultTextColor,
    ),
    bodyText2: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 14,
      fontWeight: FontWeight.w400,
      color: _defaultTextColor,
    ),
    button: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 14,
      fontWeight: FontWeight.w400,
      color: _defaultTextColor,
    ),
    overline: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 18,
      fontWeight: FontWeight.w400,
      color: _defaultTextColor,
    ),
    subtitle1: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 13,
      fontWeight: FontWeight.w400,
      color: _defaultTextColor,
    ),
    subtitle2: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 12,
      fontWeight: FontWeight.w400,
      color: _defaultTextColor,
    ),
  );
}

ThemeData baseThemeData() {
  final ColorScheme _colors = _colorScheme();
  return ThemeData.dark().copyWith(
    colorScheme: _colors,
    textTheme: _textTheme(),
    scaffoldBackgroundColor: _colors.background,
  );
}
