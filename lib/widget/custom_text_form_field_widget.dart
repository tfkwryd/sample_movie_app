import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'custom_form_field_widget.dart';

class CustomTextFormFieldWidget extends CustomFormFieldWidget<String> {
  CustomTextFormFieldWidget({
    String? label,
    String? title,
    String? hint,
    required TextController controller,
    required BuildContext context,
    bool enabled = true,
    bool mandatory = true,
    bool isObscureText = false,
    bool isEmail = false,
    bool isDigit = false,
    FormFieldValidator<String>? validator,
    double padding = 4,
    TextInputAction? textInputAction,
    Widget? suffixIcon,
    Key? key,
  }) : super(
          key: key,
          controller: controller,
          enabled: enabled,
          builder: (FormFieldState<String> state) {
            return _CustomTextForm(
              label: label,
              controller: controller,
              hint: hint,
              state: state,
              mandatory: mandatory,
              isObscureText: isObscureText,
              suffixIcon: suffixIcon,
              isEmail: isEmail,
              isDigit: isDigit,
              padding: padding,
              textInputAction: textInputAction,
            );
          },
          validator: (picker) {
            if (mandatory && (picker == null || picker.isEmpty)) {
              return 'Form tidak boleh kosong';
            }
            if (validator != null) {
              return validator(picker);
            }
            return null;
          },
        );
}

class TextController extends CustomFormFieldController<String> {
  TextController({String? initialValue}) : super(initialValue ?? '');

  @override
  String fromValue(String value) => value;

  @override
  String toValue(String text) => text;
}

class _CustomTextForm extends StatefulWidget {
  final FormFieldState<String> state;
  final TextController? controller;
  final double padding;
  final String? label;
  final String? hint;
  final bool mandatory;
  final bool isObscureText;
  final bool isEmail;
  final bool isDigit;
  final TextInputAction? textInputAction;
  final Widget? suffixIcon;

  const _CustomTextForm({
    required this.state,
    this.controller,
    this.label,
    this.hint,
    this.padding = 0,
    this.isObscureText = false,
    this.isEmail = false,
    this.mandatory = false,
    this.isDigit = false,
    this.textInputAction,
    this.suffixIcon,
  });

  @override
  State<StatefulWidget> createState() => _CustomTextFormState();
}

class _CustomTextFormState extends State<_CustomTextForm> {
  final _focusNode = FocusNode();

  String? get _hint => widget.hint;

  bool get _mandatory => widget.mandatory;

  String? get _label {
    var fullLabel = StringBuffer();
    final label = widget.label;
    if (label != null) {
      fullLabel.write(label);
      if (_mandatory) fullLabel.write(' *');
      return fullLabel.toString();
    }
    return label;
  }

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    final inputFormatters = <TextInputFormatter>[];
    TextInputType? keyboardType;

    if (widget.isDigit) {
      inputFormatters.add(FilteringTextInputFormatter.digitsOnly);
      keyboardType = TextInputType.number;
    }
    if (widget.isEmail) {
      keyboardType = TextInputType.emailAddress;
    }
    final _hint = this._hint;
    return Padding(
      padding: EdgeInsets.only(bottom: widget.padding, top: widget.padding),
      child: ConstrainedBox(
        constraints: const BoxConstraints(
          maxHeight: 300,
        ),
        child: LayoutBuilder(
          builder: (context, constraints) {
            InputBorder _border = OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
            );
            return IntrinsicHeight(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _label != null
                      ? Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 4,
                          ),
                          child: Column(
                            children: [
                              Text(_label!,
                                  style: Theme.of(context).textTheme.subtitle2),
                              const SizedBox(
                                height: 4,
                              ),
                            ],
                          ),
                        )
                      : const SizedBox.shrink(),
                  TextFormField(
                    textInputAction: widget.textInputAction,
                    keyboardType: keyboardType,
                    inputFormatters: inputFormatters,
                    focusNode: _focusNode,
                    textAlignVertical:
                        _label == null ? TextAlignVertical.center : null,
                    controller: widget.controller?.textController,
                    decoration: defaultInputDecoration.copyWith(
                      hintText: _hint != null ? _hint : 'Fill the field here',
                      hintStyle:
                          Theme.of(context).textTheme.subtitle2!.copyWith(
                                color: Theme.of(context)
                                    .colorScheme
                                    .surface
                                    .withOpacity(0.5),
                              ),
                      focusColor: Theme.of(context)
                          .colorScheme
                          .surface
                          .withOpacity(0.1),
                      fillColor: Theme.of(context)
                          .colorScheme
                          .surface
                          .withOpacity(0.1),
                      filled: true,
                      border: _border,
                      focusedBorder: _border,
                      enabledBorder: _border,
                      errorBorder: _border,
                      disabledBorder: _border,
                      isDense: _label == null,
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      errorText: widget.state.errorText,
                      alignLabelWithHint: true,
                      suffixIcon: _focusNode.hasFocus
                          ? widget.suffixIcon ??
                              IconButton(
                                icon: const Icon(Icons.cancel),
                                onPressed: () {
                                  widget.controller?.textController.clear();
                                },
                              )
                          : null,
                    ),
                    obscureText: widget.isObscureText,
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
