import 'package:flutter/material.dart';
import 'package:majootestcase/utils/constant.dart';

class CardMovieWidget extends StatelessWidget {
  final String id;
  final String? imageUrl;
  final Size imageSize;
  final String? title;
  final String? date;
  final double radius;
  final bool isSliderItem;
  final VoidCallback? onTap;
  const CardMovieWidget({
    Key? key,
    this.imageUrl,
    this.imageSize = const Size(210, 370),
    this.date,
    this.title,
    this.radius = 8,
    this.isSliderItem = false,
    required this.id,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: imageSize.width,
      child: Container(
        margin: isSliderItem
            ? const EdgeInsets.only(right: 16)
            : const EdgeInsets.all(0),
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.background,
          borderRadius: BorderRadius.circular(radius),
        ),
        child: InkWell(
          onTap: onTap,
          child: Stack(
            children: [
              SizedBox(
                height: imageSize.height,
                width: imageSize.width,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(radius),
                  ),
                  child: imageUrl != null
                      ? Image.network(
                          imageUrl.toString(),
                          fit: BoxFit.cover,
                        )
                      : Image.asset(
                          AssetPath.noImageAvailable,
                          fit: BoxFit.cover,
                        ),
                ),
              ),
              Positioned.fill(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Theme.of(context)
                              .colorScheme
                              .background
                              .withOpacity(0.5),
                          borderRadius: BorderRadius.circular(radius / 2),
                        ),
                        padding: const EdgeInsets.all(8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              title ?? '',
                              style: Theme.of(context).textTheme.headline5,
                              maxLines: 1,
                            ),
                            if (date != null)
                              Text(
                                date.toString(),
                                style: Theme.of(context).textTheme.subtitle1,
                                maxLines: 1,
                              ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
