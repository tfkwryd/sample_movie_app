import 'package:flutter/material.dart';

class InputSearchWidget extends StatelessWidget {
  final String hintText;
  final TextEditingController? controller;
  final double iconSize;
  final VoidCallback? onTap;
  final double radius;

  const InputSearchWidget({
    Key? key,
    required this.hintText,
    this.controller,
    this.iconSize = 24,
    this.onTap,
    this.radius = 16,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      style: Theme.of(context).textTheme.bodyText2,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 4,
        ),
        fillColor: Theme.of(context).colorScheme.background,
        filled: true,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius),
          borderSide: BorderSide(
            color: Theme.of(context).colorScheme.surface.withOpacity(0.5),
            width: 1,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius),
          borderSide: BorderSide(
            color: Theme.of(context).colorScheme.surface,
            width: 1.5,
          ),
        ),
        hintText: hintText,
        hintStyle: Theme.of(context).textTheme.bodyText2,
        suffixIcon: InkWell(
          onTap: onTap,
          child: Icon(
            Icons.search,
            color: Theme.of(context).colorScheme.surface,
            size: iconSize,
          ),
        ),
      ),
    );
  }
}
