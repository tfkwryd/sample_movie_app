import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/config/themes/base_theme.dart';
import 'package:majootestcase/screens/extra/loading_screen.dart';
import 'package:majootestcase/screens/home/home_screen.dart';
import 'package:majootestcase/screens/login/login_screen.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: baseThemeData(),
      home: BlocProvider(
        create: (context) => AuthBlocCubit()..fetchHistoryLogin(),
        child: MyHomePageScreen(),
      ),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(
      builder: (context, state) {
        if (state is AuthBlocLoadingState || state is AuthBlocInitialState) {
          return LoadingScreen();
        } else if (state is AuthBlocLoginState) {
          return LoginScreen();
        } else if (state is AuthBlocLoggedInState) {
          return HomeScreen();
        }
        return Center(
          child: Text(kDebugMode ? "state not implemented $state" : ""),
        );
      },
    );
  }
}
