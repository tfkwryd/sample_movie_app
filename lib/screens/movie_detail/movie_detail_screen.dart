import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response_model.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/widget/card_movie_widget.dart';

class MovieDetailScreen extends StatelessWidget {
  final Data data;
  const MovieDetailScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: _MovieDetailBody(
          data: data,
        ),
      ),
    );
  }
}

class _MovieDetailBody extends StatelessWidget {
  final Data data;
  const _MovieDetailBody({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              RotatedBox(
                quarterTurns: -1,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 24),
                  child: SizedBox(
                    width: 400,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.star,
                              size: 14,
                              color: Theme.of(context).colorScheme.surface,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              data.rank != null
                                  ? 'Rank ${data.rank.toString()}'
                                  : '-',
                              style: Theme.of(context).textTheme.headline5,
                            ),
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.calendar_view_month,
                              size: 14,
                              color: Theme.of(context).colorScheme.surface,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              data.y != null ? data.y.toString() : '-',
                              style: Theme.of(context).textTheme.headline5,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                child: SizedBox(
                  height: 450,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(16),
                      bottomRight: Radius.circular(16),
                    ),
                    child: (data.i != null && data.i?.imageUrl != null)
                        ? Image.network(
                            data.i!.imageUrl,
                            fit: BoxFit.cover,
                          )
                        : Image.asset(AssetPath.noImageAvailable),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(
            height: 24,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 24,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Text(
                        data.l.toString(),
                        textAlign: TextAlign.start,
                        style: Theme.of(context).textTheme.headline2,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 8,
                ),
                Wrap(
                  children: [
                    //Genre
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 16,
          ),
          if (data.v != null)
            SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 24),
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  ...data.v!.map(
                    (item) => CardMovieWidget(
                      imageSize: Size(300, 200),
                      isSliderItem: true,
                      id: item.id,
                      imageUrl: item.i.imageUrl,
                      title: item.l,
                    ),
                  )
                ],
              ),
            ),
          // Row(
          //   crossAxisAlignment: CrossAxisAlignment.center,
          //   children: [
          //     Expanded(
          //       child: Padding(
          //         padding: const EdgeInsets.symmetric(
          //           horizontal: 24,
          //         ),
          //         child: Column(
          //           crossAxisAlignment: CrossAxisAlignment.start,
          //           mainAxisAlignment: MainAxisAlignment.start,
          //           children: [
          //             Text(
          //               'Introduction',
          //               style: Theme.of(context).textTheme.headline3,
          //             ),
          //             const SizedBox(
          //               height: 8,
          //             ),
          //             Column(
          //               crossAxisAlignment: CrossAxisAlignment.start,
          //               children: [
          //                 Text(
          //                   '${data.l}',
          //                   style:
          //                       Theme.of(context).textTheme.bodyText1!.copyWith(
          //                             color: Theme.of(context)
          //                                 .colorScheme
          //                                 .surface
          //                                 .withOpacity(0.6),
          //                           ),
          //                 ),
          //               ],
          //             ),
          //           ],
          //         ),
          //       ),
          //     ),
          //   ],
          // ),
          const SizedBox(
            height: 24,
          ),
        ],
      ),
    );
  }
}
