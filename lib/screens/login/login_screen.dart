import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/user_model.dart';
import 'package:majootestcase/screens/extra/loading_screen.dart';
import 'package:majootestcase/screens/home/home_screen.dart';
import 'package:majootestcase/screens/register/register_screen.dart';
import 'package:majootestcase/widget/custom_button_widget.dart';
import 'package:majootestcase/widget/custom_text_form_field_widget.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginScreen> {
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthBlocCubit(),
      child: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          print(state);
          if (state is AuthBlocFailedState) {
            CoolAlert.show(
              context: context,
              type: CoolAlertType.error,
              confirmBtnColor: Theme.of(context).colorScheme.primary,
              text: "Login gagal , periksa kembali inputan anda",
            );
          }
          if (state is AuthBlocSuccesState) {
            CoolAlert.show(
              context: context,
              type: CoolAlertType.success,
              confirmBtnColor: Theme.of(context).colorScheme.primary,
              text: "Login Berhasil",
            );
          }
        },
        child: BlocBuilder<AuthBlocCubit, AuthBlocState>(
          builder: (context, state) {
            if (state is AuthBlocLoadingState) {
              return LoadingScreen();
            } else if (state is AuthBlocLoggedInState) {
              return HomeScreen();
            }
            return Scaffold(
              body: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                padding:
                    EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Selamat Datang',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                    Text(
                      'Silahkan login terlebih dahulu',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    _form(),
                    SizedBox(
                      height: 50,
                    ),
                    CustomButtonWidget(
                      text: 'Login',
                      onPressed: () => handleLogin(context),
                      height: 100,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    _register(context),
                    SizedBox(
                      height: 50,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormFieldWidget(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'example@mail.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              return pattern.hasMatch(val!)
                  ? null
                  : 'Masukkan e-mail yang valid';
            },
          ),
          SizedBox(
            height: 4,
          ),
          CustomTextFormFieldWidget(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TextButton(
          onPressed: () async {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => BlocProvider(
                  create: (context) => AuthBlocCubit(),
                  child: RegisterScreen(),
                ),
              ),
            );
          },
          child: RichText(
            text: TextSpan(
              text: 'Belum punya akun ? ',
              style: Theme.of(context).textTheme.bodyText2,
              children: [
                TextSpan(
                  text: 'Daftar',
                  style: Theme.of(context).textTheme.bodyText2!.copyWith(
                        color: Theme.of(context).colorScheme.primary,
                      ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void handleLogin(BuildContext context) async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
    if (formKey.currentState?.validate() == true) {
      UserModel user = UserModel(
        email: _email,
        password: _password,
      );
      context.read<AuthBlocCubit>().loginUser(user);
    } else {
      if (_email == '' || _password == '') {
        CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          confirmBtnColor: Theme.of(context).colorScheme.primary,
          text:
              "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan",
        );
      } else if (!pattern.hasMatch(_email)) {
        CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          confirmBtnColor: Theme.of(context).colorScheme.primary,
          text: "Masukkan e-mail yang valid",
        );
      }
    }
  }
}
