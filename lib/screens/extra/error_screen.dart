import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final bool isConnectionError;
  final Function()? retry;
  final double fontSize;
  final double gap;
  final Widget? retryButton;

  const ErrorScreen({
    Key? key,
    this.gap = 10,
    this.retryButton,
    this.message = "",
    this.fontSize = 14,
    this.retry,
    this.isConnectionError = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (isConnectionError)
              Stack(
                children: [
                  Icon(
                    Icons.network_wifi,
                    size: 100,
                    color: Theme.of(context).colorScheme.surface,
                  ),
                  Positioned(
                    top: 10,
                    right: 0,
                    child: Icon(
                      Icons.warning,
                      color: Theme.of(context).colorScheme.primary,
                      size: 30,
                    ),
                  )
                ],
              ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                message,
                style: Theme.of(context).textTheme.headline5,
                textAlign: TextAlign.center,
              ),
            ),
            retry != null
                ? Column(
                    children: [
                      retryButton ??
                          IconButton(
                            onPressed: () {
                              if (retry != null) retry!();
                            },
                            icon: Icon(
                              Icons.refresh_sharp,
                              size: 30,
                              color: Theme.of(context).colorScheme.surface,
                            ),
                          ),
                    ],
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
