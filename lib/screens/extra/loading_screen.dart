import 'package:flutter/material.dart';

class LoadingScreen extends StatelessWidget {
  final double height;
  final Color? color;

  const LoadingScreen({Key? key, this.height = 10, this.color})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(strokeWidth: 1),
      ),
    );
  }
}
