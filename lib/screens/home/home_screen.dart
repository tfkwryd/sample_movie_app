import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/widget/input_search_widget.dart';
import 'home_loaded.dart';
import '../extra/loading_screen.dart';
import '../extra/error_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: const _HomeBody(),
      ),
    );
  }
}

class _HomeBody extends StatefulWidget {
  const _HomeBody({Key? key}) : super(key: key);

  @override
  State<_HomeBody> createState() => _HomeBodyState();
}

class _HomeBodyState extends State<_HomeBody> {
  final TextEditingController _textSearchController =
      TextEditingController(text: DefaultSetting.keyword);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBlocCubit()..fetchingData(),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: BlocBuilder<HomeBlocCubit, HomeBlocState>(
              builder: (context, state) {
                return Row(
                  children: [
                    Expanded(
                      child: InputSearchWidget(
                        controller: _textSearchController,
                        hintText: 'Masukan Kata Kunci',
                        onTap: () {
                          context.read<HomeBlocCubit>().keyword =
                              _textSearchController.text;
                          context.read<HomeBlocCubit>().fetchingData();
                        },
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        context.read<AuthBlocCubit>().logoutUser();
                      },
                      icon: Icon(
                        Icons.logout,
                        size: 20,
                        color: Theme.of(context)
                            .colorScheme
                            .surface
                            .withOpacity(0.8),
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
          Expanded(
            child: BlocBuilder<HomeBlocCubit, HomeBlocState>(
              builder: (context, state) {
                if (state is HomeBlocLoadedState) {
                  return HomeLoaded(data: state.data);
                } else if (state is HomeBlocLoadingState) {
                  return LoadingScreen();
                } else if (state is HomeBlocInitialState) {
                  return LoadingScreen();
                } else if (state is HomeBlocErrorState) {
                  return ErrorScreen(
                    message: state.error,
                    retry: () => context.read<HomeBlocCubit>().fetchingData(),
                  );
                } else if (state is HomeBlocKeywordNotSetState) {
                  return _KeywordNotSet();
                } else if (state is HomeBlocConnectionErrorState) {
                  return ErrorScreen(
                    message: state.error,
                    isConnectionError: true,
                    retry: () => context.read<HomeBlocCubit>().fetchingData(),
                  );
                }
                return Center(
                    child:
                        Text(kDebugMode ? "state not implemented $state" : ""));
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _KeywordNotSet extends StatelessWidget {
  const _KeywordNotSet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('Kata kunci tidak tersedia'),
        ),
      ),
    );
  }
}
