import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response_model.dart';
import 'package:majootestcase/screens/movie_detail/movie_detail_screen.dart';
import 'package:majootestcase/widget/card_movie_widget.dart';

class HomeLoaded extends StatelessWidget {
  final List<Data> data;

  const HomeLoaded({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: GridView.builder(
          padding: EdgeInsets.all(10),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 0.5,
            crossAxisSpacing: 10.0,
            mainAxisSpacing: 10.0,
          ),
          itemCount: data.length,
          itemBuilder: (context, index) {
            return movieItemWidget(data[index], context);
          },
        ),
      ),
    );
  }

  Widget movieItemWidget(Data data, BuildContext context) {
    return CardMovieWidget(
      id: data.id,
      imageUrl: data.i != null ? data.i?.imageUrl : null,
      title: data.l,
      date: data.y != null ? data.y.toString() : null,
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => MovieDetailScreen(
              data: data,
            ),
          ),
        );
      },
    );
  }
}
